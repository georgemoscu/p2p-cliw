var local = document.getElementById('localVideo');
var remote = document.getElementById('remoteVideo');

var videoBtnContainer = document.querySelector('.right ul');
videoBtnContainer.innerHTML += '<li class="fa fa-video-camera icon" id="start-video"></li>';

var database = core.database;

// core.pc = new RTCPeerConnection(core.servers);



function sendMessage(data) {
    var msg = database.push({ sender: core.user.id, message: data });
    msg.remove();
}

function showMyFace() {
    document.getElementById('gray-manta').style.display = 'block';
    document.getElementById('video-wrapper').style.display = 'block';

    navigator.mediaDevices.getUserMedia({ audio: true, video: true })
        .then(stream => local.srcObject = stream)
        .then(stream => {
            core.pc = new RTCPeerConnection(core.servers);
            core.pc.onaddstream = (event => { console.log(event.stream); return remote.srcObject = event.stream });
            core.pc.onicecandidate = (
                event => event.candidate ?
                    core.sendMessage(JSON.stringify({ 'ice': event.candidate }))
                    : console.log("Sent All Ice")
            );
            core.pc.addStream(stream);
        }).then(_ => {
            setTimeout(() => core.pc.createOffer()
                .then(offer => core.pc.setLocalDescription(offer))
                .then(() => {
                    sendMessage(JSON.stringify({ 'sdp': core.pc.localDescription }))
                    console.log("asd", core.pc.localDescription);
                }), 2000);
        });
}

document.getElementById('start-video').addEventListener('click', showMyFace);