var Core = function () {

    this.config = {
        apiKey: "AIzaSyA-9eYR6TYvQYof_9GXVX7iIRXApCeuDBY",
        authDomain: "dawnc-6fb3e.firebaseapp.com",
        databaseURL: "https://dawnc-6fb3e.firebaseio.com",
        projectId: "dawnc-6fb3e",
        storageBucket: "dawnc-6fb3e.appspot.com",
        messagingSenderId: "891602014122"
    };

    this.firebase = firebase;
    this.firebase.initializeApp(this.config);
    this.database = this.firebase.database().ref();

    this.servers = {
        'iceServers': [
            { 'urls': 'stun:stun.services.mozilla.com' },
            { 'urls': 'stun:stun.l.google.com:19302' },
            { 'urls': 'turn:numb.viagenie.ca', 'credential': 'beaver', 'username': 'webrtc.websitebeaver@gmail.com' }
        ]
    };

    this.id = Math.floor(Date.now() / 1000);
    this.videoPC = null;
    this.chatPC = null;

    this.answerVideoCall = () => { };

    this.push = (data) => {
        if (!data) {
            data.sender = this.id;
            this.database.push(JSON.stringify(data));
        }
    }

    this.prepareCall = function() {
        return new Promise((resolve, reject) => {
            this.videoPC = new RTCPeerConnection(this.servers);
            this.videoPC.onicecandidate = this.onIceCandidateHandler;
            this.videoPC.onaddstream = this.onAddStreamHandler;
            resolve();
        });
    }

    this.onIceCandidateHandler = function(evt) {
        console.log("CANDIDATE")
        if (!evt || !evt.candidate) return;
        this.database.push(JSON.stringify({ "candidate": evt.candidate }));
    };

    this.onAddStreamHandler = function (evt) {

    };

    this.createAndSendOffer = function () {
        console.log('SENDING OFFER');
        this.videoPC.createOffer(
            (offer) => {
                var off = new RTCSessionDescription(offer);
                this.videoPC.setLocalDescription(new RTCSessionDescription(off),
                    () => {
                        this.database.push(JSON.stringify({ "sdp": off, video: true }));
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            },
            (error) => {
                console.log(error);
            }
        );
    };

    this.createAndSendAnswer = function (){
        console.log("SENDING ANSWER")
        this.videoPC.createAnswer(
            (answer) => {
                var ans = new RTCSessionDescription(answer);
                this.videoPC.setLocalDescription(ans, () => {
                    this.database.push(JSON.stringify({ "sdp": ans, video: true }));
                },
                    (error) => {
                        console.log(error);
                    }
                );
            },
            (error) => {
                console.log(error);
            }
        );
    }


    this.onvideomessage = function (evt) {
        var signal = JSON.parse(evt.val());
        console.log('signal', signal);
        if (!this.videoPC)
            if (signal.video && signal.sender != this.id) {
                this.answerVideoCall();
            }

        if (signal.sdp) {
            this.videoPC.setRemoteDescription(new RTCSessionDescription(signal.sdp));
        } else if (signal.candidate && this.videoPC && signal.sender != this.id) {
            this.videoPC.addIceCandidate(new RTCIceCandidate(signal.candidate));
        } else if (signal.closeConnection && this.videoPC) {
            this.videoPC.close();
        }
    }

    this.database.on('child_added', this.onvideomessage);

}