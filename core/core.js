/**
 * 
 */
var Core = function () {

    /**
     * 
     */
    this.config = {
        apiKey: "AIzaSyA-9eYR6TYvQYof_9GXVX7iIRXApCeuDBY",
        authDomain: "dawnc-6fb3e.firebaseapp.com",
        databaseURL: "https://dawnc-6fb3e.firebaseio.com",
        projectId: "dawnc-6fb3e",
        storageBucket: "dawnc-6fb3e.appspot.com",
        messagingSenderId: "891602014122"
    };

    //
    this.user = JSON.parse(localStorage.getItem('user')) || {};
    this.user.id = this.user.username.trim().toLowerCase() + this.user.time;

    this.friends = [];

    this.receiver = null;

    /**
     * 
     */
    this.firebase = firebase;

    //
    this.firebase.initializeApp(this.config);

    /**
     * 
     */
    this.database = this.firebase.database().ref();

    /**
     * 
     */
    this.pc = null;

    /**
     * 
     */
    this.servers = {
        'iceServers': [
            // { 'urls': 'stun:stun.services.mozilla.com' },
            { 'urls': 'stun:stun.l.google.com:19302' }
        ]
    };
    
    this.pc = new RTCPeerConnection(this.servers);
    /**
     * 
     * @param {*} data 
     */
    this.readMessage = function (data) {
        console.log(data.type);
        data = data.val();
        var sender = data.sender;
        if (sender && sender != this.user.id && !data.type) {
            var msg = JSON.parse(data.message);

            if ((msg.ice || msg.sdp) && !this.pc) {
            }

            if (msg.ice != undefined) {

                this.pc.addIceCandidate(new RTCIceCandidate(msg.ice));
                console.log("ADD ICE CANDIDATE");
            }
            else if (msg.sdp.type == "offer") {

                this.pc.setRemoteDescription(new RTCSessionDescription(msg.sdp))
                    .then(() => this.pc.createAnswer())
                    .then(answer => this.pc.setLocalDescription(answer))
                    .then(() => this.sendMessage(JSON.stringify({ 'sdp': this.pc.localDescription })));
                console.log("OFFER");

            }
            else if (msg.sdp.type == "answer") {

                this.pc.setRemoteDescription(new RTCSessionDescription(msg.sdp));
                console.log("Answer");
            }
        }

        if (data.type === 'message' && data.sender != this.user.id) {
            var messagesContainer = document.getElementById('messages');
            message = data.message.message;

            var p = document.createElement('p');
            p.className += ' chat_other';
            p.innerText = message;
            messagesContainer.appendChild(p);

            console.log(p);
        }

        if (data.type === 'new_user' && data.sender != this.user.id) {
            var exists = false;

            for (friend of this.friends) {
                exists = exists || friend.id === data.sender;
            }

            if (!exists) {
                this.friends.push(data.user);
                var user = document.createElement('div');
                user.className += ' user';
                user.innerHTML =
                    '<div class="user-short-name">' +
                    data.user.username[0] +
                    data.user.username[1] +
                    '</div>' +
                    '<div class="user-name">' +
                    data.user.username +
                    '</div>';
                user.dataset.id = data.user.id;
                user.addEventListener('click', (e, a) => {
                    this.receiver = data.user.id;
                    console.log(this.receiver);
                });
                var users = document.getElementById('users-container');
                users.appendChild(user);

                this.sendMessage({}, 'new_user');

                console.log(data);
            }
        }
    }

    /**
     * 
     * @param {*} data 
     */
    this.sendMessage = function (data, type = null) {
        console.log(data, 123);
        var obj = {
            sender: this.user.id,
            user: this.user,
            receiver: type ? this.receiver : null,
            message: data,
            type: type
        };
        msg = this.database.push(obj);
        console.log(msg);
        msg.remove();
    }

    /**
     * 
     */
    this.getModules = function () {
        var modules = JSON.parse(localStorage.getItem('modules')) || [];
        var container = document.getElementById('reference');
        if (modules) {
            for (mod of modules) {
                // Create a new container for each module
                var span = document.createElement('span');
                span.dataset.id = mod.id;
                span.dataset.name = mod.name;

                // Add stylesheet if exists
                if (mod.css) {
                    var style = document.createElement('style');
                    style.type = 'text/css';

                    if (style.styleSheet) {
                        style.styleSheet.cssText = mod.css;
                    } else {
                        style.appendChild(document.createTextNode(mod.css));
                    }
                    span.appendChild(style);
                }

                // Add html elements if exists
                if (mod.html) {
                    var html = document.createElement('span');
                    html.innerHTML = mod.html;
                    span.appendChild(html);
                }

                // Add script if exists
                if (mod.js) {
                    var script = document.createElement('script');
                    script.src = mod.js;
                    span.appendChild(script);
                }

                container.appendChild(span);
            }
        }
    }

    //
    document.getElementById('logged-user-name').innerText = 'Hi ' + this.user.username;

    //
    this.getModules();

    //
    this.database.on('child_added', data => this.readMessage(data));
}

var core;