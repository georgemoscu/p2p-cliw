var sendTextMessage;
setTimeout(() => {
    sendTextMessage = function () {
        var messagesContainer = document.getElementById('messages');
        var textInput = document.getElementById('user-message');
    
        core.sendMessage({message: textInput.value}, 'message');
    
        var p = document.createElement('p');
        p.className += ' chat_me';
        p.innerText = textInput.value;
        messagesContainer.appendChild(p);
    
        textInput.value = "";
    }
}, 2000);

