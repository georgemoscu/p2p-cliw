'use strict';

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'span' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));


var result = {
	html: '',
	css: '',
	js: '',
	name: '',
	id: ''
};

var loaded = JSON.parse(localStorage.getItem('modules')) || [];

function getHTML(element) {
	var reader = new FileReader();
	reader.addEventListener('load', () => {
		document.getElementById('html-output').innerText = reader.result;
		result.html = reader.result;
	});

	if(element.files[0]) {
		reader.readAsText(element.files[0]);
	}
}

function getCSS(element) {
	var reader = new FileReader();
	reader.addEventListener('load', () => {
		document.getElementById('css-output').innerText = reader.result;
		result.css = reader.result;
	});

	if(element.files[0]) {
		reader.readAsText(element.files[0]);
	}
}

function getJS(element) {
	var reader = new FileReader();
	reader.addEventListener('load', (ev) => {
		document.getElementById('js-output').innerText = reader.result;
		result.js = reader.result;
	});

	if(element.files[0]) {
		reader.readAsDataURL(element.files[0]);
	}
}

function getName(element) {
	var name = element.value.trim().toLowerCase().split(' ').join('_');
	for (let load of loaded) {
		if (load.id === name) {
			window.alert("This module already exists");
			element.value = '';
			return;
		}
	}	
	result.name = element.value;
	console.log(element.value);
	result.id = name;
}

function upload() {
	if (!result.name) {
		window.alert("You need to enter a name");
	} else {
		// var loaded = JSON.parse(localStorage.getItem('modules')) || [];
		loaded.push(result);
		localStorage.setItem('modules', JSON.stringify(loaded));
	}
}